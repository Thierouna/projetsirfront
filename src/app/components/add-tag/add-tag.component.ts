import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Tag } from 'src/app/models/tag.model';
import { TagserviceService } from 'src/app/services/tagservice.service';

@Component({
  selector: 'app-add-tag',
  templateUrl: './add-tag.component.html',
  styleUrls: ['./add-tag.component.css']
})
export class AddTagComponent implements OnInit {
 tagForm!: FormGroup;
 loading = false;
 submitted = false;

constructor(private ts : TagserviceService, private fb:FormBuilder){

}

  ngOnInit(): void {
    this.tagForm = this.fb.group({
      tag_name: ['', Validators.required],
      ticket: ['', Validators.required]
    })
  }

  onSubmit() : void {

  }
}
