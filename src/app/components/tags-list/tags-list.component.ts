import { Component, OnInit } from '@angular/core';
import { TagserviceService } from 'src/app/services/tagservice.service';

@Component({
  selector: 'app-tags-list',
  templateUrl: './tags-list.component.html',
  styleUrls: ['./tags-list.component.css']
})
export class TagsListComponent implements OnInit {

  tags : any;

  constructor(private ts : TagserviceService){

  }

  ngOnInit(): void {
    this.retrieveTags();
  }

  retrieveTags() : void {
    this.ts.getAll().subscribe({
      next : (data) => {
        this.tags = data;
        console.log(data)
      },
      error : (e) => {
        console.log(e)
      }
    });
  }
}
