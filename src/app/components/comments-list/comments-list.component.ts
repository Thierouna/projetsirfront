import { Component, OnInit } from '@angular/core';
import { CommentserviceService } from 'src/app/services/commentservice.service';

@Component({
  selector: 'app-comments-list',
  templateUrl: './comments-list.component.html',
  styleUrls: ['./comments-list.component.css']
})
export class CommentsListComponent implements OnInit{

  comments : any;

  constructor(private cs : CommentserviceService){

  }

  ngOnInit(): void {
    this.retrieveComments();
  }

  retrieveComments() : void {
    this.cs.getAll().subscribe({
      next : (data) => {
        this.comments = data;
        console.groupCollapsed(data);
      },
      error : (e) => console.log(e)
    });
  }
}
