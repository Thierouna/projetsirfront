import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TicketserviceService } from 'src/app/services/ticketservice.service';
import { AuthService } from 'src/app/services/auth.service';
import { first } from 'rxjs';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-add-ticket',
  templateUrl: './add-ticket.component.html',
  styleUrls: ['./add-ticket.component.css']
})
export class AddTicketComponent implements OnInit{
  
  ticketFormGroup!: FormGroup;
  loading = false;
  submitted = false;
  
  constructor(private ts:TicketserviceService, private fb : FormBuilder,
    private authenS : AuthService, private alertS: AlertService){
      
  }
  ngOnInit(): void {
    this.ticketFormGroup = this.fb.group({
      title: this.fb.control('', Validators.required),
      post: this.fb.control('', Validators.required),
      user: this.fb.control(this.authenS.currentUserValue, Validators.required)
    });
  }
  get f(){
    return this.ticketFormGroup.controls;
  }

  get v(){
    return this.ticketFormGroup.value;
  }

  onSubmit() {
   this.submitted = true;
    // reset alerts on submit
    this.alertS.clear();

    // stop here if form is invalid
    if (this.ticketFormGroup.invalid) {
        return;
    }

    this.loading = true;
    this.ts.saveBugFile(this.ticketFormGroup.value)
        .pipe(first())
        .subscribe({
          next:data => {
            this.alertS.success('Creating ticket successful', true);
            this.ticketFormGroup.reset();
            console.log(data);
        },
        error: err => {
            this.alertS.error(err);
            this.loading = false;
        }
        });
  }

}
