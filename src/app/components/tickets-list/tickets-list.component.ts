import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Ticket } from 'src/app/models/ticket.model';
import { TicketserviceService } from 'src/app/services/ticketservice.service';

@Component({
  selector: 'app-tickets-list',
  templateUrl: './tickets-list.component.html',
  styleUrls: ['./tickets-list.component.css']
})
export class TicketsListComponent implements OnInit{

  ticket$ !: Observable<Ticket[]>;

  constructor(private ts:TicketserviceService, private route:Router){

  }

  ngOnInit(): void {
    this.ticket$ = this.ts.getAll();
  }
  findTicket(ticket: Ticket){
    this.route.navigateByUrl("add-comment/"+ticket.ticketId, {state:ticket});
  }
 deleteTicket(ticket: Ticket){
  let conf = confirm("Are sure to delete this data?");
  if(!conf) return;
  this.ts.delete(ticket.ticketId);
 }
}
