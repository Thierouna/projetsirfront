import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, first } from 'rxjs';
import { Comment } from 'src/app/models/comment.model';
import { Ticket } from 'src/app/models/ticket.model';
import { AlertService } from 'src/app/services/alert.service';
import { CommentserviceService } from 'src/app/services/commentservice.service';
import { TicketserviceService } from 'src/app/services/ticketservice.service';
@Component({
  selector: 'app-add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.css']
})
export class AddCommentComponent implements OnInit {
  commentForm!: FormGroup;
  ticketForm!: FormGroup;
  submitted = false;
  loading = false;
  ticket$!: Observable<Ticket>;
  ticket_id!: number;
  ticket !: Ticket;
    constructor(private ts: TicketserviceService, private fb: FormBuilder,
      private route: Router, private alertS: AlertService,
      private activeRoute: ActivatedRoute){
        this.ticket = this.route.getCurrentNavigation()?.extras.state as Ticket;
  }

  ngOnInit(): void {
    this.ticketForm = this.fb.group({
      ticketId : this.fb.control('', Validators.required)
    });
    this.commentForm = this.fb.group({
      comment: ['', Validators.required]
    });
    this.ticket_id = this.activeRoute.snapshot.params['id'];
  }
  getTicket(){
    let ticket_id : number = this.ticketForm.value.ticketId
    this.ticket$ = this.ts.get(ticket_id)
  }
  get f(){
    return this.commentForm.controls;
  }

  get v(){
    return this.commentForm.value;
  }

  onSubmit() {
   this.submitted = true;
    // reset alerts on submit
    this.alertS.clear();

    // stop here if form is invalid
    if (this.commentForm.invalid) {
        return;
    }

    this.loading = true;
    this.ts.saveBugFile(this.commentForm.value)
        .pipe(first())
        .subscribe(
            data => {
                this.alertS.success('comment adding successful', true);
                this.route.navigateByUrl("tickets-list");
            },
            error => {
                this.alertS.error(error);
                this.loading = false;
            });
}
}
