import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Comment } from '../models/comment.model';
import { Observable } from 'rxjs';

const baseUrl = 'http://localhost:8182/api/comment';

@Injectable({
  providedIn: 'root'
})
export class CommentserviceService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Array<Comment>> {
    return this.http.get<Array<Comment>>(`${baseUrl}/all`);
  }

  get(id: number): Observable<Comment> {
    return this.http.get<Comment>(`${baseUrl}/${id}`)
  }

  create(data: Comment): Observable<Comment> {
    return this.http.post<Comment>(baseUrl, data);
  }

  update(id: number, data: Comment): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id: number): Observable<number> {
    return this.http.delete<number>(`${baseUrl}/${id}`);
  }
}
