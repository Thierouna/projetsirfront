import { Injectable } from '@angular/core';
import { Observable, catchError, throwError } from 'rxjs';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Ticket } from '../models/ticket.model';
const baseUrl = 'http://localhost:8182/api/ticket';

const herder = new HttpHeaders().set('Content-Type', 'application/json')
.set('Accept', 'application/json');
@Injectable({
  providedIn: 'root'
})
export class TicketserviceService {

  constructor(private http: HttpClient) { }
 errorHandler(error:any){
  console.log(error);
 }

  getAll(): Observable<Array<Ticket>> {
    return this.http.get<Array<Ticket>>(`${baseUrl}/all`);
  }

  get(id: number): Observable<Ticket> {
    return this.http.get<Ticket>(`${baseUrl}/${id}`);
  }


  saveBugFile(data: Ticket): Observable<Ticket> {
    return this.http.post<Ticket>(`${baseUrl}/bug/file`, {data}, {headers:herder});
  }

  saveFeatureRequestFile(data: Ticket): Observable<Ticket> {
    return this.http.post<Ticket>(`${baseUrl}/feature/request/file`, {data}, {headers:herder})
    .pipe(
      catchError(e=>throwError(this.errorHandler(e))
    )
  }

  update(id: any, data: any): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, {data});
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`);
  }
  getTicketComment(): Observable<Ticket> {
    return this.http.get<Ticket>(`${baseUrl}/comment`);
  }
}
