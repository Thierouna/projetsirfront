import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Tag } from '../models/tag.model';

const baseUrl = 'http://localhost:8182/api/tag'

@Injectable({
  providedIn: 'root'
})
export class TagserviceService {

  constructor(private http:HttpClient) { }

  getAll():Observable<Tag[]>{
    return this.http.get<Tag[]>(`${baseUrl}/all`);
  }

  get(id:any):Observable<Tag>{
    return this.http.get<Tag>(`${baseUrl}/${id}`);
  }

  create(data:any):Observable<any>{
    return this.http.post(baseUrl, data);
  }

  update(id:any, data:any):Observable<any>{
    return this.http.post(`${baseUrl}/${id}`, data);
  }

  delete(id:any):Observable<any>{
    return this.http.delete(`${baseUrl}/${id}`);
  }
}
