import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';

const baseUrl = 'http://localhost:8182/api/user'
@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  constructor(private http:HttpClient) { }

  getAll():Observable<Array<User>>{
    return this.http.get<User[]>(`${baseUrl}/all`);
  }

  get(id:any):Observable<User>{
    return this.http.get<User>(`${baseUrl}/${id}`);
  }

  create(data : any) : Observable<any>{
    return this.http.post(`${baseUrl}/signup`, data);
  }

  upgrade(id : any, data : any) : Observable<any>{
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id:any) : Observable<any>{
    return this.http.delete(`${baseUrl}/${id}`);
  }
  
}