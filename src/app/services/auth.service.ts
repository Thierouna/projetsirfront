import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { BehaviorSubject, Observable, map } from 'rxjs';
import { User } from '../models/user.model';

const baseUrl = 'http://localhost:8182/api/user'

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new 
    BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')!));
    this.currentUser = this.currentUserSubject.asObservable();
  }
  get currentUserValue(): User{
    return this.currentUserSubject.value;
  }
  login(username: string, passcode: string){
    return this.http.post<any>(`${baseUrl}/login`, {username, passcode})
    .pipe(map(user=>{
      localStorage.setItem('currentUser', JSON.stringify(user));
      this.currentUserSubject.next(user);
      return user;
    }));
  }
  logout(){
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null!);
  }
}