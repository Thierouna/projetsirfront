import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddUserComponent } from './components/add-user/add-user.component';
import { AddCommentComponent } from './components/add-comment/add-comment.component';
import { AddTagComponent } from './components/add-tag/add-tag.component';
import { AddTicketComponent } from './components/add-ticket/add-ticket.component';
import { AddFileComponent } from './components/add-file/add-file.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UsersDetailsComponent } from './components/users-details/users-details.component';
import { TicketsDetailsComponent } from './components/tickets-details/tickets-details.component';
import { TicketsListComponent } from './components/tickets-list/tickets-list.component';
import { CommentsListComponent } from './components/comments-list/comments-list.component';
import { CommentsDetailsComponent } from './components/comments-details/comments-details.component';
import { TagsDetailsComponent } from './components/tags-details/tags-details.component';
import { TagsListComponent } from './components/tags-list/tags-list.component';
import { FilesListComponent } from './components/files-list/files-list.component';
import { FilesDetailsComponent } from './components/files-details/files-details.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { AuthGuard } from './_helpers/http.interceptor';
const routes: Routes = [
  { path: 'add-user', component: AddUserComponent },
  { path: 'add-ticket', component: AddTicketComponent },
  { path: 'add-comment/:id', component: AddCommentComponent },
  { path: 'add-tag', component: AddTagComponent },
  { path: 'add-file', component: AddFileComponent },
  {path: 'login', component: LoginComponent},
  {path: '', component: HomeComponent, canActivate: [AuthGuard]},
  {path:"users-list", component:UsersListComponent},
  {path:'tickets-list', component:TicketsListComponent},
  {path:'comments-list', component:CommentsListComponent},
  {path:'tags-list', component:TagsListComponent},
  {path:'files-list', component:FilesListComponent},

  {path:'user-details', component:UsersDetailsComponent},
  {path:'ticket-details', component:TicketsDetailsComponent},
  {path:'comment-details', component:CommentsDetailsComponent},
  {path:'tag-details', component:TagsDetailsComponent},
  {path:'file-details', component:FilesDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
