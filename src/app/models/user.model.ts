import { Comment } from "./comment.model";
import { Ticket } from "./ticket.model";

export interface User {
    userId: number;
    username: string;
    firstname: string;
    lastname: string;
    email: string;
    passcode: string;
    ticketList: Ticket[];
    messageList: Comment[];
}
