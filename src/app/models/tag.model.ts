import { Ticket } from "./ticket.model";

export interface Tag {
    id : number;
    tag_name : string;
    ticket : Ticket;
}
