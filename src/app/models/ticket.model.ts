import { Comment } from "./comment.model";
import { Tag } from "./tag.model";

export interface Ticket {
    ticketId: number;
    title: string;
    post: string;
    commentList: Comment[];
    tagList: Tag[];
    typeFile: string;
}
