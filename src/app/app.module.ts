import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddUserComponent } from './components/add-user/add-user.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { UsersDetailsComponent } from './components/users-details/users-details.component';
import { TicketsDetailsComponent } from './components/tickets-details/tickets-details.component';
import { TicketsListComponent } from './components/tickets-list/tickets-list.component';
import { AddTicketComponent } from './components/add-ticket/add-ticket.component';
import { AddCommentComponent } from './components/add-comment/add-comment.component';
import { CommentsListComponent } from './components/comments-list/comments-list.component';
import { CommentsDetailsComponent } from './components/comments-details/comments-details.component';
import { TagsDetailsComponent } from './components/tags-details/tags-details.component';
import { TagsListComponent } from './components/tags-list/tags-list.component';
import { AddTagComponent } from './components/add-tag/add-tag.component';
import { AddFileComponent } from './components/add-file/add-file.component';
import { FilesListComponent } from './components/files-list/files-list.component';
import { FilesDetailsComponent } from './components/files-details/files-details.component';
import { AuthGuard } from './_helpers/http.interceptor';
import { AlertComponent } from './components/alert/alert.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { UserserviceService } from './services/userservice.service';
import { TicketserviceService } from './services/ticketservice.service';
import { CommentserviceService } from './services/commentservice.service';
import { AlertService } from './services/alert.service';
import { TagserviceService } from './services/tagservice.service';
import { AuthService } from './services/auth.service';

@NgModule({
  declarations: [
    AppComponent,
    AddUserComponent,
    UsersListComponent,
    UsersDetailsComponent,
    TicketsDetailsComponent,
    TicketsListComponent,
    AddTicketComponent,
    AddCommentComponent,
    CommentsListComponent,
    CommentsDetailsComponent,
    TagsDetailsComponent,
    TagsListComponent,
    AddTagComponent,
    AddFileComponent,
    FilesListComponent,
    FilesDetailsComponent,
    AlertComponent,
    LoginComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [AuthGuard, UserserviceService, TicketserviceService, CommentserviceService,
  TagserviceService, AlertService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
